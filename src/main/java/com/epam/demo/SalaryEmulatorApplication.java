package com.epam.demo;

import com.epam.demo.models.employee.Employee;
import com.epam.demo.models.position.Position;
import com.epam.demo.models.salary.Salary;
import com.epam.demo.models.salary.SalaryCurrency;
import com.epam.demo.services.employee.EmployeeService;
import com.epam.demo.services.position.PositionService;
import com.epam.demo.services.salary.SalaryService;
import com.epam.demo.utils.SalaryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@ImportResource({"classpath*:spring-config.xml"})
public class SalaryEmulatorApplication {
    private Logger LOG = LoggerFactory.getLogger(SalaryEmulatorApplication.class);

    @Autowired
    private SalaryUtil salaryUtil;

    public static void main(String[] args) {
        SpringApplication.run(SalaryEmulatorApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(final EmployeeService employeeService, final PositionService positionService,
                             final SalaryService salaryService){
        return args -> {
            generateEmployee(employeeService);

//            1 year
            firstYear(employeeService);
//            2 year
            secondYear(positionService);
        };
    }

    private void generateEmployee(EmployeeService employeeService) {
        List<Employee> employees = new ArrayList<>();

        Salary junSalary = new Salary(SalaryCurrency.USD, BigDecimal.valueOf(250));
        Position junEngineer = new Position("Junior SOftwaer Engineer", salaryUtil.convertToByn(junSalary));

        Salary midSalary = new Salary(SalaryCurrency.USD, BigDecimal.valueOf(500));
        Position midEngineer = new Position("SOftwaer Engineer", salaryUtil.convertToByn(midSalary));

        Salary sinSalary = new Salary(SalaryCurrency.USD, BigDecimal.valueOf(1000));
        Position sinEngineer = new Position("Sinior SOftwaer Engineer", salaryUtil.convertToByn(sinSalary));

        Salary leadSalary = new Salary(SalaryCurrency.USD, BigDecimal.valueOf(1500));
        Position leadEngineer = new Position("Lead SOftwaer Engineer", salaryUtil.convertToByn(leadSalary));

        Salary manSalary = new Salary(SalaryCurrency.USD, BigDecimal.valueOf(2500));
        Position manager = new Position("Manager", salaryUtil.convertToByn(manSalary));

        Employee employee_1 = new Employee("Andrew", junEngineer);
        Employee employee_2 = new Employee("Georg", junEngineer);
        Employee employee_3 = new Employee("Ea", midEngineer);
        Employee employee_4 = new Employee("Yuanik", midEngineer);
        Employee employee_5 = new Employee("An", midEngineer);
        Employee employee_6 = new Employee("Gena", midEngineer);
        Employee employee_7 = new Employee("Korol", sinEngineer);
        Employee employee_8 = new Employee("Gena1", sinEngineer);
        Employee employee_9 = new Employee("Nick", leadEngineer);
        Employee employee_0 = new Employee("Lera", manager);

        employees.add(employee_1);
        employees.add(employee_2);
        employees.add(employee_3);
        employees.add(employee_4);
        employees.add(employee_5);
        employees.add(employee_6);
        employees.add(employee_7);
        employees.add(employee_8);
        employees.add(employee_9);
        employees.add(employee_0);

        employeeService.addAllEmployee(employees);

        LOG.info("Employee creating has been done.");
    }

    private void firstYear(EmployeeService employeeService){
        employeeService.fireEmployeeForPosition("Junior SOftwaer Engineer");

        Employee newbee = new Employee();
        newbee.setName("Kostya");

        employeeService.hireEmployeeForPosition(newbee, "Junior SOftwaer Engineer");
    }

    private void secondYear(PositionService positionService){
        Salary testSalary = new Salary(SalaryCurrency.USD, BigDecimal.valueOf(250));
        Position testEngineer = new Position("Testing Engineer", salaryUtil.convertToByn(testSalary));

        Salary baSalary = new Salary(SalaryCurrency.USD, BigDecimal.valueOf(250));
        Position ba = new Position("Business analyst", salaryUtil.convertToByn(baSalary));

        positionService.addPosition(testEngineer);
        positionService.addPosition(ba);

        Position manager = positionService.getPositionByName("Manager");
        manager.setName("Project Manager");
        positionService.updatePosition(manager);
    }
}
