package com.epam.demo.dao.repositories;

import com.epam.demo.models.employee.Employee;
import com.epam.demo.models.position.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    void removeEmployeeByPosition(Position position);
}
