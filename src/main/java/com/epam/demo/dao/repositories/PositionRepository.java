package com.epam.demo.dao.repositories;

import com.epam.demo.models.position.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, Long> {
    Position findPositionByName(String name);
}
