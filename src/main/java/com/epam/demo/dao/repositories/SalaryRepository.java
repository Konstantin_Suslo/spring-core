package com.epam.demo.dao.repositories;

import com.epam.demo.models.salary.Salary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface SalaryRepository extends JpaRepository<Salary, Long> {

}
