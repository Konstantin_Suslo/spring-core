package com.epam.demo.dao.salary;


import com.epam.demo.models.salary.Salary;

import java.util.List;
import java.util.Optional;

public interface SalaryDao {
    void addSalary(Salary salary);
    Optional<Salary> findSalaryById(Long id);
    Salary updateSalary(Salary salary);
    void removeSalaryById(Long id);
    List<Salary> findAllSalaries();
}
