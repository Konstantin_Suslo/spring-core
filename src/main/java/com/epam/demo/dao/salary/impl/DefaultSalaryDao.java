package com.epam.demo.dao.salary.impl;

import com.epam.demo.dao.repositories.SalaryRepository;
import com.epam.demo.dao.salary.SalaryDao;
import com.epam.demo.models.salary.Salary;

import java.util.List;
import java.util.Optional;

public class DefaultSalaryDao implements SalaryDao {
    private final SalaryRepository salaryRepository;

    public DefaultSalaryDao(SalaryRepository salaryRepository) {
        this.salaryRepository = salaryRepository;
    }

    @Override
    public void addSalary(Salary salary) {
        salaryRepository.saveAndFlush(salary);
    }

    @Override
    public Optional<Salary> findSalaryById(Long id) {
        return salaryRepository.findById(id);
    }

    @Override
    public Salary updateSalary(Salary salary) {
        return salaryRepository.save(salary);
    }

    @Override
    public void removeSalaryById(Long id) {
        salaryRepository.deleteById(id);
    }

    @Override
    public List<Salary> findAllSalaries() {
        return salaryRepository.findAll();
    }
}
