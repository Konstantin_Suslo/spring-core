package com.epam.demo.dao.employee.impl;

import com.epam.demo.dao.employee.EmployeeDao;
import com.epam.demo.dao.repositories.EmployeeRepository;
import com.epam.demo.models.employee.Employee;
import com.epam.demo.models.position.Position;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public class DefaultEmployeeDao implements EmployeeDao {

    private final EmployeeRepository employeeRepository;

    public DefaultEmployeeDao(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    @Override
    public void addEmployee(Employee employee) {
        employeeRepository.saveAndFlush(employee);
    }

    @Override
    public void addAllEmployee(Iterable<Employee> employees) {
        employeeRepository.saveAll(employees);
    }

    @Override
    public Optional<Employee> findEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void removeEmployeeById(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public List<Employee> findAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public void removeEmployeeByPosition(Position position) {
        employeeRepository.removeEmployeeByPosition(position);
    }
}
