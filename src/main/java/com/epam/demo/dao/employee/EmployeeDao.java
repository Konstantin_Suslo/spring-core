package com.epam.demo.dao.employee;

import com.epam.demo.models.employee.Employee;
import com.epam.demo.models.position.Position;

import java.util.List;
import java.util.Optional;

public interface EmployeeDao {
    void addEmployee(Employee employee);
    void addAllEmployee(Iterable<Employee> employees);
    Optional<Employee> findEmployeeById(Long id);
    Employee updateEmployee(Employee employee);
    void removeEmployeeById(Long id);
    List<Employee> findAllEmployees();

    void removeEmployeeByPosition(Position position);
}
