package com.epam.demo.dao.position.impl;

import com.epam.demo.dao.position.PositionDao;
import com.epam.demo.dao.repositories.PositionRepository;
import com.epam.demo.models.position.Position;

import java.util.List;
import java.util.Optional;

public class DefaultPositionDao implements PositionDao {
    private final PositionRepository positionRepository;

    public DefaultPositionDao(PositionRepository positionRepository) {
        this.positionRepository = positionRepository;
    }

    @Override
    public void addPosition(Position position) {
        positionRepository.saveAndFlush(position);
    }

    @Override
    public Optional<Position> findPositionById(Long id) {
        return positionRepository.findById(id);
    }

    @Override
    public Position findPositionByName(String name) {
        return positionRepository.findPositionByName(name);
    }

    @Override
    public Position updatePosition(Position position) {
        return positionRepository.save(position);
    }

    @Override
    public void removePositionById(Long id) {
        positionRepository.deleteById(id);
    }

    @Override
    public List<Position> findAllPositions() {
        return positionRepository.findAll();
    }


}
