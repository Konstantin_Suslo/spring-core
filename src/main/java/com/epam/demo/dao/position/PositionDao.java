package com.epam.demo.dao.position;


import com.epam.demo.models.position.Position;

import java.util.List;
import java.util.Optional;

public interface PositionDao {
    void addPosition(Position position);
    Optional<Position> findPositionById(Long id);
    Position findPositionByName(String name);
    Position updatePosition(Position position);
    void removePositionById(Long id);
    List<Position> findAllPositions();
}
