package com.epam.demo.services.position;

import com.epam.demo.models.position.Position;

import java.util.List;

public interface PositionService {
    void addPosition(Position position);
    Position getPositionById(Long id);
    Position getPositionByName(String name);
    Position updatePosition(Position position);
    void removePositionById(Long id);
    List<Position> getAllPositions();
}
