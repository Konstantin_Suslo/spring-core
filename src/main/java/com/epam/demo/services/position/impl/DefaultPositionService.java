package com.epam.demo.services.position.impl;

import com.epam.demo.dao.position.PositionDao;
import com.epam.demo.models.position.Position;
import com.epam.demo.services.position.PositionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public class DefaultPositionService implements PositionService {
    private final Logger LOG = LoggerFactory.getLogger(DefaultPositionService.class);

    private PositionDao positionDao;

    public DefaultPositionService(PositionDao positionDao) {
        this.positionDao = positionDao;
    }

    @Override
    public void addPosition(Position position) {
        positionDao.addPosition(position);
        LOG.info("Position " + position.getName() + " has been added.");
    }

    @Override
    public Position getPositionById(Long id) {
        final Optional<Position> position = positionDao.findPositionById(id);
        if (position.isPresent()){
            return position.get();
        } else {
            throw new IllegalArgumentException("The position is not exist");
        }
    }

    @Override
    public Position getPositionByName(String name) {
        return positionDao.findPositionByName(name);
    }

    @Override
    public Position updatePosition(Position position) {
        LOG.info("Position " + position.getId() + " has been updated.");
        return positionDao.updatePosition(position);
    }

    @Override
    public void removePositionById(Long id) {
        positionDao.removePositionById(id);
        LOG.info("Positon " + id + " has been removed.");
    }

    @Override
    public List<Position> getAllPositions() {
        return positionDao.findAllPositions();
    }
}
