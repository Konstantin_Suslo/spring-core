package com.epam.demo.services.employee;

import com.epam.demo.models.employee.Employee;
import com.epam.demo.models.position.Position;

import java.util.List;

public interface EmployeeService {
    void addEmployee(Employee employee);
    void addAllEmployee(List<Employee> employeeList);

    void hireEmployeeForPosition(Employee employee, String positionName);
    void fireEmployeeForPosition(String positionName);
}
