package com.epam.demo.services.employee.impl;

import com.epam.demo.dao.employee.EmployeeDao;
import com.epam.demo.dao.position.PositionDao;
import com.epam.demo.models.employee.Employee;
import com.epam.demo.models.position.Position;
import com.epam.demo.services.employee.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class DefaultEmployeeService implements EmployeeService {
    private Logger LOG = LoggerFactory.getLogger(DefaultEmployeeService.class);

    private EmployeeDao employeeDao;
    private PositionDao positionDao;

    public DefaultEmployeeService(EmployeeDao employeeDao, PositionDao positionDao) {
        this.employeeDao = employeeDao;
        this.positionDao = positionDao;
    }

    @Override
    public void addEmployee(Employee employee) {
        employeeDao.addEmployee(employee);
        LOG.info("Employee " + employee.getName() + " has been added.");
    }

    @Override
    public void addAllEmployee(List<Employee> employeeList) {
        employeeDao.addAllEmployee(employeeList);
        LOG.info("Employees have been added.");
    }

    @Override
    public void hireEmployeeForPosition(Employee employee, String positionName) {
        final Position position = positionDao.findPositionByName(positionName);
        employee.setPosition(position);
        employeeDao.addEmployee(employee);
        LOG.info("Employee " + employee.getName() + " has been hired for " + positionName + " position.");
    }

    @Override
    public void fireEmployeeForPosition(String positionName) {
        final Position position = positionDao.findPositionByName(positionName);
        employeeDao.removeEmployeeByPosition(position);
        LOG.info("All employees with " + positionName + " position have been fired.");
    }
}
