package com.epam.demo.services.salary;

import com.epam.demo.models.salary.Salary;

import java.math.BigDecimal;

public interface SalaryService {
    void addSalary(Salary salary);

    void recountSalariesWithCurrentCourse();
}
