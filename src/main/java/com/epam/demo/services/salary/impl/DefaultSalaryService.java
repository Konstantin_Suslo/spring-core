package com.epam.demo.services.salary.impl;

import com.epam.demo.dao.salary.SalaryDao;
import com.epam.demo.models.salary.Salary;
import com.epam.demo.models.salary.SalaryCurrency;
import com.epam.demo.services.salary.SalaryService;
import com.epam.demo.utils.SalaryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class DefaultSalaryService implements SalaryService {
    private final Logger LOG = LoggerFactory.getLogger(DefaultSalaryService.class);

    private final SalaryDao salaryDao;
    private final SalaryUtil salaryUtil;

    public DefaultSalaryService(SalaryDao salaryDao, SalaryUtil salaryUtil) {
        this.salaryDao = salaryDao;
        this.salaryUtil = salaryUtil;
    }

    @Override
    public void addSalary(Salary salary) {
        salaryDao.addSalary(salary);
    }

    @Override
    public void recountSalariesWithCurrentCourse() {
        List<Salary> salaryList = salaryDao.findAllSalaries().stream()
                .filter(salary -> salary.getCurrency().equals(SalaryCurrency.USD))
                .collect(Collectors.toList());

        salaryList.forEach(this::recountSalary);
        LOG.info("Salaries have been recalculated.");
    }

    private void recountSalary(Salary salary){
        salary.setCurrency(SalaryCurrency.BYN);
        salary.setValue(salaryUtil.convertToByn(salary.getValue()));
    }
}
