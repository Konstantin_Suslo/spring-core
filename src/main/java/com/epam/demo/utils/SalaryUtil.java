package com.epam.demo.utils;

import com.epam.demo.models.salary.Salary;
import com.epam.demo.models.salary.SalaryCurrency;
import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class SalaryUtil {
    private final BigDecimal currencyCourseMultiplier;

    public SalaryUtil(BigDecimal currencyCourseMultiplier) {
        this.currencyCourseMultiplier = currencyCourseMultiplier;
    }

    public BigDecimal convertToByn(BigDecimal usdSalary){
        return usdSalary.multiply(currencyCourseMultiplier);
    }

    public Set<Salary> convertToByn(Salary usdSalary){
        Set<Salary> salaries = new HashSet<>();
        Salary bynSalary = new Salary(SalaryCurrency.BYN, convertToByn(usdSalary.getValue()));

        salaries.add(usdSalary);
        salaries.add(bynSalary);

        return salaries;
    }
}
