package com.epam.demo.models.salary;

import com.epam.demo.models.position.Position;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "salary")
public class Salary implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "CURRENCY")
    @NotNull
    private SalaryCurrency currency;

    @Column(name = "VALUE")
    private BigDecimal value;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Position position;

    public Salary(){
        super();
    }

    public Salary(@NotNull SalaryCurrency currency, BigDecimal value) {
        this.currency = currency;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SalaryCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(SalaryCurrency currency) {
        this.currency = currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
