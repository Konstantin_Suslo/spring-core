package com.epam.demo.models.salary;

public enum SalaryCurrency {
    BYN, USD
}
